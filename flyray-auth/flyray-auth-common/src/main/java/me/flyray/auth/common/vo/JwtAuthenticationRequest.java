package me.flyray.auth.common.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class JwtAuthenticationRequest implements Serializable {

	private static final long serialVersionUID = -8445943548965154778L;

	private String username;
	private String nickname;
	private String userId;
	private String password;
	private String platformId;
	private String platformKey;
	private String saltKey;
	/**
	 * 登录类型
	 * 空或01：用户名密码登录
	 * 02：手机号登录
	 */
	private String loginType;
	/**
	 * 登录手机号
	 */
	private String phone;

	public JwtAuthenticationRequest(String username, String password, String platformId, String saltKey,String platformKey) {
		this.username = username;
		this.password = password;
		this.platformId = platformId;
		this.platformKey = platformKey;
		this.saltKey = saltKey;
	}
	
	public JwtAuthenticationRequest() {
	}
}
