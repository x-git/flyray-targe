package me.flyray.crm.facade.crm.core;

public class Test {
	
	public static void main(String[] args) {
		String checksum = MD5.sign("258.00", "BSV00123", "utf-8");
		String checksum1 = MD5.sign("258", "", "utf-8");
		System.out.println("checksum:"+checksum);
		System.out.println("checksum1:"+checksum1);
	}

}
