package me.flyray.crm.facade.crm.core;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.UnsupportedEncodingException;
import java.security.SignatureException;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

/**
 * 功能：MD5签名加密处理
 * 说明：
 */
public class MD5 {
    /**
     * 签名字符串
     *
     * @param text          需要签名的字符串
     * @param key           密钥
     * @param input_charset 编码格式
     * @return 签名结果
     */
    public static String sign(String text, String key, String input_charset) {
        text = text + key;
        return DigestUtils.md5Hex(getContentBytes(text, input_charset));
    }

    /**
     * 签名字符串
     *
     * @param text          需要签名的字符串
     * @param sign          签名结果
     * @param key           密钥
     * @param input_charset 编码格式
     * @return 签名结果
     */
    public static boolean verify(String text, String sign, String key, String input_charset) {
        text = text + key;
        String mysign = DigestUtils.md5Hex(getContentBytes(text, input_charset));
        if (mysign.equals(sign)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * MD5加密
     *
     * @param text
     * @return
     */
    public static String md5(String text) {
        String md5 = DigestUtils.md5Hex(getContentBytes(text, "utf-8"));
        //return md5.toUpperCase();
        return md5;
    }

    /**
     * @param content
     * @param charset
     * @return
     * @throws SignatureException
     * @throws UnsupportedEncodingException
     */
    private static byte[] getContentBytes(String content, String charset) {
        if (charset == null || "".equals(charset)) {
            return content.getBytes();
        }
        try {
            return content.getBytes(charset);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("MD5签名过程中出现错误,指定的编码集不对,您目前指定的编码集是:" + charset);
        }
    }
    
    /**
     * 获取盐值
     * */
    public static String generateSaltkey(){
    	  Random r = new Random();  
          StringBuilder sb = new StringBuilder(16);  
          sb.append(r.nextInt(99999999)).append(r.nextInt(99999999));  
          int len = sb.length();  
          if (len < 16) {  
              for (int i = 0; i < 16 - len; i++) {  
                  sb.append("0");  
              }  
          }  
          return sb.toString();  
    }
    
    public static void main(String[] args) {
		System.out.println(generateSaltkey());
	}
}
