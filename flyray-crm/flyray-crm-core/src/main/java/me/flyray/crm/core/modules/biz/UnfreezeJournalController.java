package me.flyray.crm.core.modules.biz;

import me.flyray.common.context.BaseContextHandler;
import me.flyray.common.exception.ApiException;
import me.flyray.common.msg.ResponseCode;
import me.flyray.common.util.FlyrayBeanUtils;
import me.flyray.common.util.Query;
import me.flyray.crm.core.entity.UnfreezeJournal;
import me.flyray.crm.facade.request.customerAccountJournalRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import me.flyray.common.msg.TableResultResponse;
import me.flyray.common.rest.BaseController;
import me.flyray.crm.core.biz.UnfreezeJournalBiz;

import java.util.Map;

@RestController
@RequestMapping("unfreezeJournal")
public class UnfreezeJournalController extends BaseController<UnfreezeJournalBiz, UnfreezeJournal> {

	private static final Logger logger= LoggerFactory.getLogger(UnfreezeJournalController.class);
	/**
	 * 查询解冻流水表
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public TableResultResponse<UnfreezeJournal> queryUnfreezeJournal(@RequestBody customerAccountJournalRequest param){
		param.setPlatformId(setPlatformId(BaseContextHandler.getPlatformId()));
		try {
			Map<String, Object> regMap = FlyrayBeanUtils.objectFiledNotEmptyToMap(param);
			if (regMap.size() <= 0){
				throw new ApiException(ResponseCode.INVALID_FIELDS);
			}
			Query query = new Query(regMap);
			return baseBiz.selectPageSortNoDeleteFlag(query,"create_time");
		}catch (Exception e){
			throw new ApiException(ResponseCode.INVALID_FIELDS.getCode(),e.getMessage());
		}
	}
}