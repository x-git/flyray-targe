package me.flyray.crm.core.biz.customer;

import me.flyray.crm.core.entity.CustomerMessageRel;
import me.flyray.crm.core.mapper.CustomerMessageRelMapper;
import org.springframework.stereotype.Service;

import me.flyray.common.biz.BaseBiz;

/**
 * 用户消息关联表
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-08-21 17:35:50
 */
@Service
public class CustomerMessageRelBiz extends BaseBiz<CustomerMessageRelMapper, CustomerMessageRel> {
}