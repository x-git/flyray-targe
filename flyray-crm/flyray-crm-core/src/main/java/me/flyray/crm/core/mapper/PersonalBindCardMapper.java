package me.flyray.crm.core.mapper;

import me.flyray.crm.core.entity.PersonalBindCard;
import tk.mybatis.mapper.common.Mapper;

/**
 * 用户绑卡表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-08-16 16:56:09
 */
@org.apache.ibatis.annotations.Mapper
public interface PersonalBindCardMapper extends Mapper<PersonalBindCard> {
	
}
