package me.flyray.crm.core.feignserver;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import me.flyray.common.msg.BaseApiResponse;
import me.flyray.crm.facade.request.AccountJournalQueryRequest;
import me.flyray.crm.facade.request.AccountQueryRequest;
import me.flyray.crm.facade.response.AccountJournalQueryResponse;
import me.flyray.crm.facade.response.CustomerAccountQueryResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import me.flyray.crm.core.biz.customer.CustomerAccountQueryBiz;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/***
 * 账户查询
 * */
@Api(tags="个人或者商户账户查询")
@Controller
@RequestMapping("feign/customer")
public class FeignCustomerAccountQuery {
	
	@Autowired
	private CustomerAccountQueryBiz commonAccountQueryBiz;
	
	/**
	 * 账户查询
	 * @param 
	 * @return withdrawApply
	 */
	@ApiOperation("账户查询")
	@RequestMapping(value = "/accountQuery",method = RequestMethod.POST)
    @ResponseBody
    public BaseApiResponse<List<CustomerAccountQueryResponse>> accountQuery(@RequestBody @Valid AccountQueryRequest accountQueryRequest){
		List<CustomerAccountQueryResponse> response = commonAccountQueryBiz.accountQuery(accountQueryRequest);
        return BaseApiResponse.newSuccess(response);
    }
	
	/**
	 * 查询账户交易流水
	 * @author centerroot
	 * @time 创建时间:2018年9月27日下午4:10:22
	 * @param accountJournalQueryRequest
	 * @return
	 */
	@ApiOperation("查询账户交易流水")
	@RequestMapping(value = "/accountJournalQuery",method = RequestMethod.POST)
    @ResponseBody
    public BaseApiResponse<List<AccountJournalQueryResponse>> accountJournalQuery(@RequestBody @Valid AccountJournalQueryRequest accountJournalQueryRequest){
		List<AccountJournalQueryResponse> response = commonAccountQueryBiz.accountJournalQuery(accountJournalQueryRequest);
		return BaseApiResponse.newSuccess(response);
    }
	
}
