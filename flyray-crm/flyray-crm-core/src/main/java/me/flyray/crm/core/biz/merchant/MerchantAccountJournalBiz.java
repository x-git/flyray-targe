package me.flyray.crm.core.biz.merchant;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import me.flyray.common.biz.BaseBiz;
import me.flyray.common.msg.TableResultResponse;
import me.flyray.crm.core.entity.MerchantAccountJournal;
import me.flyray.crm.core.mapper.MerchantAccountJournalMapper;
import me.flyray.crm.facade.request.customerAccountJournalRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

import java.util.List;

/**
 * 商户账户流水（充、转、提、退、冻结流水）
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Service
public class MerchantAccountJournalBiz extends BaseBiz<MerchantAccountJournalMapper, MerchantAccountJournal> {
	
	private static final Logger logger= LoggerFactory.getLogger(MerchantAccountJournalBiz.class);

}