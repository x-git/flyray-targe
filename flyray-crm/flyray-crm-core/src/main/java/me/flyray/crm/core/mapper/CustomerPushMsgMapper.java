package me.flyray.crm.core.mapper;

import me.flyray.crm.core.entity.CustomerPushMsg;
import tk.mybatis.mapper.common.Mapper;

/**
 * 客户消息推送参数表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-08-21 17:35:49
 */
@org.apache.ibatis.annotations.Mapper
public interface CustomerPushMsgMapper extends Mapper<CustomerPushMsg> {
	
}
