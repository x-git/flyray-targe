package me.flyray.crm.core.biz;

import java.util.List;

import me.flyray.crm.core.entity.FreezeJournal;
import me.flyray.crm.core.mapper.FreezeJournalMapper;
import me.flyray.crm.facade.request.customerAccountJournalRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import me.flyray.common.biz.BaseBiz;
import me.flyray.common.msg.TableResultResponse;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 冻结流水表
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Service
public class FreezeJournalBiz extends BaseBiz<FreezeJournalMapper, FreezeJournal> {
	
	private static final Logger logger= LoggerFactory.getLogger(FreezeJournalBiz.class);
	
}