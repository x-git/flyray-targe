package me.flyray.crm.facade.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("商户费率接口参数")
public class MerChannelFeeRequest {
	@NotNull(message = "平台编号不能为空")
	@ApiModelProperty("平台编号")
	private String platformId;
	
	@ApiModelProperty("商户编号")
	private String merchantId;

	@NotNull(message = "支付通道名称编号不能为空")
	@ApiModelProperty("支付通道名称编号")
	private String payChannelNo;

	@NotNull(message = "支付金额不能为空")
	@ApiModelProperty("支付金额")
	private String payAmt;
	
}
