package me.flyray.crm.facade.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * 个人账户信息
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "个人账户信息请求参数")
public class PersonalAccountRequest implements Serializable {
	@ApiModelProperty(value = "当前页码")
	private int page;

	@ApiModelProperty(value = "每页条数")
	private int limit;
	//序号
	@ApiModelProperty(value = "序号")
    private Integer id;
	
	    //账户编号
	@ApiModelProperty(value = "账户编号")
    private String accountId;
	
	    //平台编号
	@NotNull(message="平台编号不能为空")
	@ApiModelProperty(value = "平台编号")
    private String platformId;
	
	    //个人信息编号
	@ApiModelProperty(value = "个人信息编号")
    private String personalId;
	
	    //账户类型  ACC001：余额账户，ACC002：红包账户，ACC003：积分账户，ACC004：手续费账户，ACC005：已结算账户，ACC006：交易金额账户，ACC007：冻结金额账户，ACC008：平台管理费账户，ACC009：平台服务费账户等......
	@ApiModelProperty(value = "账户类型")
    private String accountType;
	
	    //币种  CNY：人民币
	@ApiModelProperty(value = "币种")
    private String ccy;
	
	    //账户余额
	@ApiModelProperty(value = "账户余额")
    private BigDecimal accountBalance;
	
	    //冻结金额
	@ApiModelProperty(value = "冻结金额")
    private BigDecimal freezeBalance;
	
	    //校验码（余额加密值）
	@ApiModelProperty(value = "校验码")
    private String checkSum;
	
	    //账户状态 00：正常，01：冻结
	@ApiModelProperty(value = "账户状态")
    private String status;
	
	    //创建时间
	@ApiModelProperty(value = "创建时间")
    private Date createTime;
	
	    //更新时间
	@ApiModelProperty(value = "更新时间")
    private Date updateTime;
	
}
