package me.flyray.crm.facade.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 用户实名信息查询
 * @author centerroot
 * @time 创建时间:2018年9月25日上午10:42:38
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "用户实名信息查询")
public class CustomerRealNameRequest implements Serializable {

	@ApiModelProperty("平台编号")
	private String platformId;
	
	@ApiModelProperty(value = "个人客户编号")
    private String personalId;

	@NotNull(message="页数不能为空")
	@ApiModelProperty(value = "页数")
	private int page;
	
	@NotNull(message="每页条数不能为空")
	@ApiModelProperty(value = "每页条数")
	private int limit;

}
