package me.flyray.crm.facade.response;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author: bolei
 * @date: 15:09 2019/1/11
 * @Description: 类描述
 */

@Data
public class FreezeJournalResponse implements Serializable {

    //流水号
    private String journalId;

    //平台编号
    private String platformId;

    //客户类型    CUST00：平台   CUST01：商户  CUST02：用户
    private String customerType;

    //个人或商户编号
    private String customerNo;

    //交易类型  01：充值，02：提现，
    private String tradeType;

    //订单号
    private String orderNo;

    //冻结金额
    private BigDecimal freezeBalance;

    //冻结状态 1：已冻结  2：部分解冻  3：已解冻
    private String status;

    //创建时间
    private Date createTime;

    //更新时间
    private Date updateTime;

    //账户类型
    private String accountType;
}
