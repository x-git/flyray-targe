package me.flyray.crm.facade.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("账户流水查询接口参数")
public class AccountJournalQueryRequest implements Serializable {

	@NotNull(message = "平台编号不能为空")
	@ApiModelProperty("平台编号")
	private String platformId;

	@NotNull(message = "客户编号不能为空")
	@ApiModelProperty("客户编号")
	private String customerId;

	@ApiModelProperty("商户编号")
	private String merchantId;
	
	@ApiModelProperty("商户类型编号")
	private String merchantType;
	
	@ApiModelProperty("个人客户编号")
	private String personalId;	
	
	@ApiModelProperty("账户类型")
	private String accountType;
	
	@NotNull(message = "客户类型不能为空")
	@ApiModelProperty("客户类型")
	private String customerType;
	
}
