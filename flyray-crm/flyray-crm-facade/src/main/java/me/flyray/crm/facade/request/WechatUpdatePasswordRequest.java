package me.flyray.crm.facade.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import me.flyray.crm.facade.BaseRequest;

import javax.validation.constraints.NotNull;

/**
 * 小程序修改交易密码
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "修改交易密码")
public class WechatUpdatePasswordRequest extends BaseRequest {

	@NotNull(message = "平台编号不能为空")
	@ApiModelProperty("平台编号")
	private String platformId;
	
	@NotNull(message = "用户编号不能为空")
	@ApiModelProperty(value = "用户编号")
	private String customerId;
	
	@NotNull(message = "个人编号不能为空")
	@ApiModelProperty(value = "个人用户编号")
	private String personalId;
	
	@NotNull(message="手机号不能为空")
	@ApiModelProperty(value = "手机号")
	private String phone;
	
	@NotNull(message="手机验证码不能为空")
	@ApiModelProperty(value = "手机验证码")
	private String smsCode;
	
	@NotNull(message="新密码不能为空")
	@ApiModelProperty(value = "新密码")
	private String newPassword;
	
	@NotNull(message="再次输入新密码不能为空")
	@ApiModelProperty(value = "再次输入新密码")
	private String confirmPassword;

}
