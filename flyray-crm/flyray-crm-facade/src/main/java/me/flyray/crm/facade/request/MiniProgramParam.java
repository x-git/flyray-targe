package me.flyray.crm.facade.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("用户信息参数")
public class MiniProgramParam implements Serializable {
	
	@NotNull(message="平台编号不能为空")
	@ApiModelProperty("平台编号")
	private String platformId;
	
	@NotNull(message="用户账号不能为空")
	@ApiModelProperty("用户账号")
	private String customerId;
	
	@NotNull(message="头像不能为空")
	@ApiModelProperty("头像")
	private String avatar;
	
	@NotNull(message="昵称不能为空")
	@ApiModelProperty("昵称")
	private String nickName;

}
