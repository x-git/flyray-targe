package me.flyray.crm.facade.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


/**
 * 查询账户流水
 * @author chj
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "查询账户流水请求参数")
public class customerAccountJournalRequest implements Serializable {

	@ApiModelProperty(value = "当前页码")
	private Integer page;

	@ApiModelProperty(value = "每页条数")
	private Integer limit;
	
	//平台编号
	@ApiModelProperty(value = "平台编号")
    private String platformId;
	
	//交易类型   01：支付   02：退款  03：提现  04：充值
	@ApiModelProperty(value = "交易类型")
    private String tradeType;
	
	//订单号
	@ApiModelProperty(value = "订单号")
    private String orderNo;
	
	//订单号
	@ApiModelProperty(value = "账务流水号")
	private String journalId;
	

}
