package me.flyray.crm.facade.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Auther: luya
 * @Date: 2019-01-17 20:10
 * @Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "运营查询流水请求参数")
public class MerchantAccountOpJournalRequest implements Serializable {

    @ApiModelProperty(value = "当前页码")
    private int page = 1;

    @ApiModelProperty(value = "每页条数")
    private int limit =10 ;

    //账户编号
    @ApiModelProperty(value = "账户编号")
    private String customerId;

    //流水号
    @ApiModelProperty(value = "流水号")
    private String journalId;

    //交易类型
    @ApiModelProperty(value = "交易类型")
    private String tradeType;

    //平台编号
    @NotNull(message="平台编号不能为空")
    @ApiModelProperty(value = "平台编号")
    private String platformId;

    @ApiModelProperty(value = "商户名称")
    private String merchantName;

    /**
     * 开始时间，仅当时间周期类型为2才生效
     */
    @ApiModelProperty(value = "开始时间")
    private String startDate;
    /**
     * 开始时间，仅当时间周期类型为2才生效
     */
    @ApiModelProperty(value = "结束时间")
    private String endDate;

}
