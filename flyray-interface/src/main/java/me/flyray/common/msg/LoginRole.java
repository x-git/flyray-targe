package me.flyray.common.msg;

/**
 * 短信通知类型
 */
public enum LoginRole {

	personal("01","个人"),
    merchant("02","商户");

    private String code;
    private String desc;

    private LoginRole(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
