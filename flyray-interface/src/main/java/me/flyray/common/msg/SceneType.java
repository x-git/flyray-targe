package me.flyray.common.msg;

/**
 * 场景编号
 * @author Administrator
 *
 */
public enum SceneType {
	
	PHONE_RECHARGE("BT0004","手机充值"),
	MEDICAL_PAYMENT("BT0007","医疗缴费"),
	REGISTER_PAYMENT("BT0008","挂号缴费"),
	ADD_READER("BT0009","办理读者证"),
	UP_READER("BT0011","读者证升级"),
	WATER_PAYMENT("BT0017","水费缴费"),
	ELECTRICITY_PAYMENT("BT0018","电费缴费"),
	GAS_PAYMENT("BT0019","煤气费缴费"),
	VIDEO_CARD("BT0020","视频卡充值"),
	FLOW_PAYMENT("BT0021","流量充值"),
	MEALCARD_RECHARGE("BT0022","餐卡充值"),
	ZB_RECHARGE("BT0023","中百卡充值"),
	OUTPATIENT_RECHARGE("BT0024","门诊充值");
	
	private String code;
	private String name;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	private SceneType(String code, String name) {
		this.code = code;
		this.name = name;
	}
	
	public static SceneType getSceneType(String code) {
		for (SceneType s : SceneType.values()) {
			if (s.getCode().equals(code)) {
				return s;
			}
		}
		return null;
	}
	
	
	

}
