package me.flyray.common.enums;

/** 
* 交易类型（消费:01，退款:02，提现:03，充值:04，转账:05）
*/

public enum TradeType {

    PAYRECEIPT("01","消费"),
	REFUND("02","退款"),
    WITHDREW("03","提现"),
	RECHARGE("04","充值"),
    PRERECEIPT("05","预收款"),
    RECEIVE("06","营收"),
    TRANSFER("07","转账"),
    SETTLEMENT("08","结算");

    private String code;
    private String desc;
    
    private TradeType(String code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
    
    public static TradeType getCommentModuleNo(String code){
        for(TradeType o : TradeType.values()){
            if(o.getCode().equals(code)){
                return o;
            }
        }
        return null;
    }
}
