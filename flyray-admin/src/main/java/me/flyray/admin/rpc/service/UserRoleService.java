package me.flyray.admin.rpc.service;

import me.flyray.admin.entity.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import me.flyray.admin.biz.UserRoleBiz;

/** 
* @author: bolei
* @date：2018年4月17日 上午9:55:51 
* @description：类说明
*/

@Service
public class UserRoleService {
	
	@Autowired
	private UserRoleBiz userRoleBiz;

	public void addUserRole(UserRole userRole) {
		userRoleBiz.insertSelective(userRole);
	}

}
