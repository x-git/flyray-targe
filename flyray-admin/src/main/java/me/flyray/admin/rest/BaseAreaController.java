package me.flyray.admin.rest;

import me.flyray.admin.biz.BaseAreaBiz;
import me.flyray.admin.entity.BaseArea;
import me.flyray.admin.vo.AreaTree;
import me.flyray.admin.vo.BaseAreaRequestParam;
import me.flyray.common.msg.BaseApiResponse;
import me.flyray.common.msg.ResponseCode;
import me.flyray.common.rest.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 行政区域
 * @author Administrator
 *
 */
@RestController
@RequestMapping("baseArea")
public class BaseAreaController extends BaseController<BaseAreaBiz, BaseArea> {
	
	private static final Logger logger = LoggerFactory.getLogger(BaseAreaController.class);
	
	/**
	 * 查询树形目录
	 * @param param
	 * @return
	 */
	@RequestMapping(value="/tree", method=RequestMethod.GET)
	@ResponseBody
	public List<AreaTree> select(@RequestParam Map<String, Object> param) {
		logger.info("查询行政区域树。。。{}"+param);
		String areaCode = (String) param.get("areaCode");
		return baseBiz.listAreaTree(areaCode);
	}
	
	
	/**
	 * 区域列表
	 * @param param
	 * @return
	 */
	@RequestMapping(value="/list", method=RequestMethod.GET)
	@ResponseBody
	public List<BaseArea> listAll(@RequestParam Map<String, Object> param) {
		logger.info("查询行政区域列表。。。{}"+param);
		return baseBiz.listAreaByParentCode(param);
	}
	
	
	/**
	 * 添加区域
	 * @param param
	 * @return
	 */
	@RequestMapping(value="/add", method=RequestMethod.POST)
	@ResponseBody
	public BaseApiResponse addArea(@RequestBody BaseAreaRequestParam param) {
		BaseArea area = baseBiz.addBaseArea(param);
		if (area == null) {
			return BaseApiResponse.newSuccess();
		}else {
			return BaseApiResponse.newFailure(ResponseCode.REGION_NO_EXIST);
		}
	}
	
	/**
	 * 删除区域
	 * @param param
	 * @return
	 */
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	@ResponseBody
	public BaseApiResponse deleteArea(@RequestBody BaseAreaRequestParam param) {
		BaseArea area = baseBiz.deleteBaseArea(param);
		if (area != null) {
			return BaseApiResponse.newSuccess(area);
		}else {
			return BaseApiResponse.newFailure(ResponseCode.REGION_NO_EXIST);
		}
	}
	
	
	/**
	 * 修改区域
	 * @param param
	 * @return
	 */
	@RequestMapping(value="/update", method=RequestMethod.POST)
	@ResponseBody
	public BaseApiResponse updateArea(@RequestBody BaseAreaRequestParam param) {
		BaseArea area = baseBiz.updateBaseArea(param);
		if (area != null) {
			return BaseApiResponse.newSuccess(area);
		}else {
			return BaseApiResponse.newFailure(ResponseCode.REGION_NO_EXIST);
		}
	}
	
	/**
	 * 详情
	 * @param param
	 * @return
	 */
	@RequestMapping(value="/info", method=RequestMethod.POST)
	@ResponseBody
	public BaseApiResponse queryInfo(@RequestBody BaseAreaRequestParam param) {
		BaseArea area = baseBiz.areaInfo(param);
		if (area != null) {
			return BaseApiResponse.newSuccess(area);
		}else {
			return BaseApiResponse.newFailure(ResponseCode.REGION_NO_EXIST);
		}
	}
	
	/**
	 * 查询省份
	 * @param param
	 * @return
	 */
	@RequestMapping(value="/province", method=RequestMethod.POST)
	@ResponseBody
	public BaseApiResponse queryProvinces(@RequestBody BaseAreaRequestParam param) {
		List<BaseArea> list = baseBiz.queryProvinces(param);
		return BaseApiResponse.newSuccess(list);
	}
	
	
	
	
	
	
}