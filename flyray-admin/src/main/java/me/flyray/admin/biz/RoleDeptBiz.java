package me.flyray.admin.biz;

import me.flyray.admin.entity.RoleDept;
import me.flyray.admin.mapper.RoleDeptMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import me.flyray.common.biz.BaseBiz;

/** 
* @author: bolei
* @date：2018年4月8日 下午4:14:33 
* @description：类说明
*/

@Service
@Transactional(rollbackFor = Exception.class)
public class RoleDeptBiz extends BaseBiz<RoleDeptMapper, RoleDept> {

}
