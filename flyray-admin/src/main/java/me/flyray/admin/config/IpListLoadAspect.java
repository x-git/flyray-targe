package me.flyray.admin.config;

import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;

import me.flyray.admin.mapper.BaseIpListMapper;
import me.flyray.cache.CacheProvider;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;


/**
 * 动态记载白名单信息
 */
@Component
@Aspect
@Slf4j
@Service
public class IpListLoadAspect {
	@Autowired 
	private BaseIpListMapper maper;
	/*@Autowired 
	private CacheAPI cacheApi;*/
	@AfterReturning(value="execution(* me.flyray.admin.mapper.BaseIpListMapper.update*(..)) or "
			+ "execution(* me.flyray.admin.mapper.BaseIpListMapper.insert*(..)) or "
			+ "execution(* me.flyray.admin.mapper.BaseIpListMapper.delete*(..))"
			, argNames="rtv", returning="rtv")
	public void loadIpList(JoinPoint jp, Object rtv){
		 log.info("重新加载ip白名单开始");
		 List<Map<String, Object>> ipList = maper.getAllBaseIpList();
		 if(null!=ipList&&ipList.size()>0){
			// cacheApi.set("ipList", ipList, 9999999);
			 CacheProvider.set("i_flyray-admin:ipList", ipList);
	    	}
		 log.info("重新加载ip白名单结束");
	}

	
}
