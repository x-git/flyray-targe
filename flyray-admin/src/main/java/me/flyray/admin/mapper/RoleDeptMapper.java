package me.flyray.admin.mapper;

import me.flyray.admin.entity.RoleDept;
import tk.mybatis.mapper.common.Mapper;

public interface RoleDeptMapper extends Mapper<RoleDept> {
}